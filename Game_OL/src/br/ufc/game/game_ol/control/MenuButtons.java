package br.ufc.game.game_ol.control;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;

import br.ufc.game.game_ol.config.Assets;
import br.ufc.game.game_ol.scenes.GameScene;

import static br.ufc.game.game_ol.config.DeviceSettings.screenHeight;
import static br.ufc.game.game_ol.config.DeviceSettings.screenWidth;
import static br.ufc.game.game_ol.config.DeviceSettings.screenResolution;

public class MenuButtons extends CCLayer implements ButtonDelegate{
	private Button playButton;
	private GameScene delegate;
	
	
	public MenuButtons(){
		this.setIsTouchEnabled(true);
		this.playButton = new Button(Assets.PLAY);
		
		// coloca bot�es na posi��o correta
		setButtonsPositions();
		
		addChild(playButton);
		
		this.playButton.setDelegate(this);
	}

	private void setButtonsPositions() {
		playButton.setPosition(screenResolution(CGPoint.ccp(screenWidth()/2, screenHeight()-250)));
	}

	@Override
	public void buttonClicked(Button sender) {
		if(sender.equals(this.playButton)){
			System.out.println("Button clicked: Play");
			
			CCDirector.sharedDirector().replaceScene(CCFadeTransition.transition(1.0f, GameScene.createGame()));
		}
	}
	
	public void setDelegate(GameScene gameScene) {
		this.delegate = gameScene;
	}
}
