package br.ufc.game.game_ol.control;

import org.cocos2d.events.CCTouchDispatcher;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import android.view.MotionEvent;

public class Button extends CCLayer{

	private CCSprite buttonImage;
	private ButtonDelegate delegate;
	
	//informando ao Cocos2D que teremos objetos do tipo bot�o,
	//que receber�o uma imagem e s�o habilitados para serem tocado
	public Button(String buttonImage) {
		this.setIsTouchEnabled(true);
		this.buttonImage = CCSprite.sprite(buttonImage);
		addChild(this.buttonImage);
	}
	
	public void setDelegate(ButtonDelegate sender){
		this.delegate = sender;
	}
	
	// Ao tocar no layer, botao sera avisado que foi tocado
	protected void registerWithTouchDispacher(){
		CCTouchDispatcher.sharedDispatcher().addTargetedDelegate(this, 0, false);
	}
	
	public boolean ccTouchesBegan(MotionEvent event){
		CGPoint touchLocation = CGPoint.make(event.getX(), event.getY());
		touchLocation = CCDirector.sharedDirector().convertToGL(touchLocation);
		touchLocation = this.convertToNodeSpace(touchLocation);
		
		// Verificando o toque no bot�o
		if(CGRect.containsPoint(this.buttonImage.getBoundingBox(), touchLocation)){
			delegate.buttonClicked(this);
		}
		
		return true;
	}
}
