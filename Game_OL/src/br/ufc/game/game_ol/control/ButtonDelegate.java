package br.ufc.game.game_ol.control;

public interface ButtonDelegate {
	public void buttonClicked(Button sender);
}
