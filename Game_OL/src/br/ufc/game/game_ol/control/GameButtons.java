package br.ufc.game.game_ol.control;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.types.CGPoint;

import br.ufc.game.game_ol.config.Assets;
import br.ufc.game.game_ol.scenes.GameScene;
import static br.ufc.game.game_ol.config.DeviceSettings.screenResolution;
import static br.ufc.game.game_ol.config.DeviceSettings.screenWidth;

public class GameButtons extends CCLayer implements ButtonDelegate{
	private Button leftControl;
	private Button rightControl;
	private Button shootButton; 
	private GameScene delegate;
	
	public static GameButtons gameButtons(){
		return new GameButtons();
	}
	
	public GameButtons(){
		//habilita o toque na tela
		this.setIsTouchEnabled(true);
		
		//criando os botoes
		this.leftControl = new Button(Assets.LEFT);
		this.rightControl = new Button(Assets.RIGHT);
		this.shootButton = new Button(Assets.SHOOTBUTTON);
		
		// Configuracao das delega�oes
		this.leftControl.setDelegate(this);
		this.rightControl.setDelegate(this);
		this.shootButton.setDelegate(this);
		
		// Configurando as posicoes dos buttons
		setButtonspPosition();
		
		// Adicionando os botoes na tela
		addChild(leftControl);
		addChild(rightControl);
		addChild(shootButton);
	}
	
	// Posicao dos buttons
	private void setButtonspPosition() {
		leftControl.setPosition(screenResolution(CGPoint.ccp(40, 40)));
		rightControl.setPosition(screenResolution(CGPoint.ccp(100, 40)));
		shootButton.setPosition(screenResolution(CGPoint.ccp(screenWidth()-40, 40)));
	}

	@Override
	public void buttonClicked(Button sender) {
		if (sender.equals(this.leftControl)) {
			this.delegate.moveLeft();
		}
		
		if (sender.equals(this.rightControl)) {
			this.delegate.moveRight();
		}
		
		// detecta o toque no bot�o de tiro e chama o disparo 
		if (sender.equals(this.shootButton)) {
			this.delegate.shoot();
		}
	}

	public void setDelegate(GameScene gameScene) {
		this.delegate = gameScene;
	}
}
