package br.ufc.game.game_ol.engine;

import java.util.Random;

import org.cocos2d.layers.CCLayer;

import br.ufc.game.game_ol.config.Assets;
import br.ufc.game.game_ol.interfaces.PedrasEngineDelegate;
import br.ufc.game.game_ol.objects.Pedras;

public class PedrasEngine extends CCLayer{
	private PedrasEngineDelegate delegate;
	
	public PedrasEngine(){
		this.schedule("pedrasEngine", 1.0f / 10f);
	}

	public void pedrasEngine(float dt){
		if(new Random().nextInt(30) == 0){
			 this.getDelegate().createPedra(new Pedras(Assets.PEDRAS));
		}
	}
	
	public void setDelegate(PedrasEngineDelegate delegate) {
		this.delegate = delegate;
	}
	
	public PedrasEngineDelegate getDelegate() {
		return delegate;
	}
}
