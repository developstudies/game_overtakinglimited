package br.ufc.game.game_ol.objects;

import java.util.Random;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCFadeOut;
import org.cocos2d.actions.interval.CCScaleBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

import br.ufc.game.game_ol.interfaces.PedrasEngineDelegate;

import static br.ufc.game.game_ol.config.DeviceSettings.screenWidth;
import static br.ufc.game.game_ol.config.DeviceSettings.screenHeight;
import static br.ufc.game.game_ol.config.DeviceSettings.screenResolution;

public class Pedras extends CCSprite{
	private float x, y;
	private PedrasEngineDelegate delegate;
	
	public Pedras(String image){
		super(image);
		x = new Random().nextInt(Math.round(screenWidth()));
		y = screenHeight();
	}
	
	public void start(){
		this.schedule("update");
	}
	
	public void update(float dt){
		y -= 1;
		this.setPosition(screenResolution(CGPoint.ccp(x, y)));
	}
	
	public void setDelegate(PedrasEngineDelegate delegate) {
		this.delegate = delegate;
	}
	
	public void shooted() {
		this.delegate.removePedra(this);
		
		// para de ficar chamando o update
		//this.unschedule("update");
		float dt = 0.2f;
		CCScaleBy a1 = CCScaleBy.action(dt, 0.5f);
		CCFadeOut a2 = CCFadeOut.action(dt);
		CCSpawn s1 = CCSpawn.actions(a1, a2);
		CCCallFunc c1 = CCCallFunc.action(this, "removeMe");
		this.runAction(CCSequence.actions(s1, c1));
	}
	
	public void removeMe() {
		this.removeFromParentAndCleanup(true);
	}
}
