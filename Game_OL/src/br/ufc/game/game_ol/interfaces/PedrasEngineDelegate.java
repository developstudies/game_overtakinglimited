package br.ufc.game.game_ol.interfaces;

import br.ufc.game.game_ol.objects.Pedras;

public interface PedrasEngineDelegate {	

	public void removePedra(Pedras pedras);

	public void createPedra(Pedras pedra);
}
