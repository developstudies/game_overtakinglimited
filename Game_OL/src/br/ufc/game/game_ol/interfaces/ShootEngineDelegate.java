package br.ufc.game.game_ol.interfaces;

import br.ufc.game.game_ol.objects.Shoot;

public interface ShootEngineDelegate {
	public void createShoot(Shoot shoot);

	public void removeShoot(Shoot shoot);
}

