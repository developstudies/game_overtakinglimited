package br.ufc.game.game_ol.scenes;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.types.CGPoint;

import br.ufc.game.game_ol.config.Assets;
import br.ufc.game.game_ol.control.MenuButtons;
import br.ufc.game.game_ol.screen.ScreenBackground;
import static br.ufc.game.game_ol.config.DeviceSettings.screenHeight;
import static br.ufc.game.game_ol.config.DeviceSettings.screenWidth;
import static br.ufc.game.game_ol.config.DeviceSettings.screenResolution;

public class TitleScreen extends CCLayer{

	private ScreenBackground background;
	
	public CCScene scene() {
		CCScene scene = CCScene.node();
		scene.addChild(this);
		return scene;
	}
	
	public TitleScreen() {
		
		// background
		this.background = new ScreenBackground(Assets.BACKGROUND_FASE1);
		this.background.setPosition(screenResolution(CGPoint.ccp(screenWidth() / 2.0f, screenHeight() / 2.0f)));
		this.addChild(this.background);
		
		// buttons
		MenuButtons menuLayer = new MenuButtons();
		this.addChild(menuLayer);
	}
}
