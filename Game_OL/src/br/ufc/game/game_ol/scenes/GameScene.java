package br.ufc.game.game_ol.scenes;

import java.lang.reflect.InvocationTargetException;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

import br.ufc.game.game_ol.config.Runner;
import br.ufc.game.game_ol.screen.FinalScreen;
import br.ufc.game.game_ol.config.Assets;
import br.ufc.game.game_ol.control.GameButtons;
import br.ufc.game.game_ol.engine.PedrasEngine;
import br.ufc.game.game_ol.objects.Pedras;
import br.ufc.game.game_ol.objects.Player;
import br.ufc.game.game_ol.objects.Score;

import br.ufc.game.game_ol.objects.Shoot;
import br.ufc.game.game_ol.interfaces.PedrasEngineDelegate;
import br.ufc.game.game_ol.screen.ScreenBackground;
import br.ufc.game.game_ol.interfaces.ShootEngineDelegate;

import static br.ufc.game.game_ol.config.DeviceSettings.screenHeight;
import static br.ufc.game.game_ol.config.DeviceSettings.screenWidth;
import static br.ufc.game.game_ol.config.DeviceSettings.screenResolution;

/**
 * A classe @GameScene tem muita responsabilidade, por�m n�o det�m regras e l�gicas de cada elemento. 
 * Outra fun��o importante dessa classe � aplicar um dos conceitos do game loop.
*/
public class GameScene extends CCLayer implements PedrasEngineDelegate, ShootEngineDelegate{
	private ScreenBackground background;
	private PedrasEngine pedrasEngine;
	private Pedras pedras;
	private CCLayer pedrasLayer;
	private CCLayer playerLayer;
	private CCLayer shootsLayer;
	private Player player;
	private ShootEngineDelegate delegate;
	private CCLayer scoreLayer;
	private Score score;
	
	
	
	@SuppressWarnings("rawtypes")
	private ArrayList pedrasArray;
	@SuppressWarnings("rawtypes")
	private List playersArray;
	@SuppressWarnings("rawtypes")
	private ArrayList shootsArray;

	public GameScene() {
		this.background = new ScreenBackground(Assets.BACKGROUND_FASE1);
		this.background.setPosition(screenResolution(CGPoint.ccp(screenWidth() / 2.0f, screenHeight() / 2.0f)));
		this.addChild(this.background);
		
		GameButtons gameButtonsLayer = GameButtons.gameButtons();
		gameButtonsLayer.setDelegate(this);
		addChild(gameButtonsLayer);
		
		// Criando os Layers
		this.pedrasLayer = CCLayer.node();
		this.playerLayer = CCLayer.node();
		this.shootsLayer = CCLayer.node();
		this.scoreLayer = CCLayer.node();
		
		this.addGameObjects();
		
		// add Layers
		this.addChild(this.pedrasLayer);
		this.addChild(this.playerLayer);		
		this.addChild(this.shootsLayer);		
		this.addChild(this.scoreLayer);
		
		this.setIsTouchEnabled(true);
	}
	
	public static CCScene createGame(){
		CCScene scene = CCScene.node();
		GameScene layer = new GameScene();
		scene.addChild(layer);
		return scene;
	}
	
	public void addGameObjects(){
		this.pedrasArray = new ArrayList();
		this.pedrasEngine = new PedrasEngine();
		
		this.player = new Player();
		this.playerLayer.addChild(player);
		
		this.score = new Score();
		this.score.setDelegate(this);
		this.scoreLayer.addChild(this.score);
		
		this.playersArray = new ArrayList();
		this.playersArray.add(this.player);
		
		this.shootsArray = new ArrayList();
		this.player.setDelegate(this);
		
		
	}
	
	public void startGame() {

		// Set Game Status
		// PAUSE
		Runner.check().setGamePlaying(true);
		Runner.check().setGamePaused(false);

		// pause
		SoundEngine.sharedEngine().setEffectsVolume(1f);
		SoundEngine.sharedEngine().setSoundVolume(1f);

		// startgame
		this.schedule("checkHits");

		this.startEngines();
	}
	
	public void onEnter(){
		super.onEnter();
		this.schedule("checkHits");
		this.startEngines();
	}
	
	private void startEngines(){
		this.addChild(pedrasEngine);
		this.pedrasEngine.setDelegate(this);
	}
	
	public CGRect getBoarders(CCSprite object) {
		CGRect rect = object.getBoundingBox();
		CGPoint GLpoint = rect.origin;
		CGRect GLrect = CGRect.make(GLpoint.x, GLpoint.y, rect.size.width, rect.size.height);

		return GLrect;
	}
	
	private boolean checkRadiusHitsOfArray(List<? extends CCSprite> array1, List<? extends CCSprite> array2, 
			GameScene gameScene, String hit) {
		
		boolean result = false;
		for (int i = 0; i < array1.size(); i++) {
			// Pega objeto do primeiro array
			CGRect rect1 = getBoarders(array1.get(i));
			for (int j = 0; j < array2.size(); j++) {
			
				// Pega objeto do segundo array
				CGRect rect2 = getBoarders(array2.get(j));
				
				// Verifica colis�o
				if (CGRect.intersects(rect1, rect2)) {
					System.out.println("Colision Detected: " + hit);
					result = true;
					Method method;
					
					try {
						method = GameScene.class.getMethod(hit,	CCSprite.class, CCSprite.class);
						method.invoke(gameScene, array1.get(i), array2.get(j));
					} catch (SecurityException e1) {
						e1.printStackTrace();
					} catch (NoSuchMethodException e1) {
						e1.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}
			return result;
	}
	
	// Colisao em tempo em tempos. Agendamento do Framework
	public void checkHits(float dt) {
		this.checkRadiusHitsOfArray(this.pedrasArray, this.shootsArray, this, "pedraHit");
		this.checkRadiusHitsOfArray(this.pedrasArray, this.playersArray, this, "playerHit");
	}
	
	
	public void pedraHit(CCSprite pedra, CCSprite shoot) {
		((Pedras) pedra).shooted();
		((Shoot) shoot).explode();
		this.score.increase();
	}

	@Override
	public void createPedra(Pedras pedra){
		pedra.setDelegate(this);
		this.pedrasLayer.addChild(pedra);
		pedra.start();
		this.pedrasArray.add(pedra);
	}
	
	@Override
	public void removePedra(Pedras pedras) {
		this.pedrasArray.remove(pedras);
	}
	
	@Override
	public void removeShoot(Shoot shoot){
		this.shootsArray.remove(shoot);
	}

	@Override
	public void createShoot(Shoot shoot) {
		this.shootsLayer.addChild(shoot);
		shoot.setDelegate(delegate);
		shoot.start();
		this.shootsArray.add(shoot);
	}
	
	public void playerHit(CCSprite pedra, CCSprite player) {
		((Pedras) pedra).shooted();
		((Player) player).explode();
	}
	
	public boolean shoot() {
		player.shoot();
		return true;
	}
	
	public void moveLeft() {
		player.moveLeft();
	}
	
	public void moveRight() {
		player.moveRight();
	}
	
	public void startFinalScreen() {
		CCDirector.sharedDirector().replaceScene(new FinalScreen().scene());
	}
}
