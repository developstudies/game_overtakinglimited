package br.ufc.game.game_ol.config;

public class Assets {
	public static String BACKGROUND_FASE1 = "background_fase1.png";
	public static String PLAY = "play.png";
	public static String PEDRAS = "meteor.png";
	public static String PRINCIPE = "principe.png";
	public static String RIGHT = "right.png";
	public static String LEFT = "left.png";
	public static String SHOOTBUTTON = "shootButton.png";
	public static String SHOOT = "shoot.png";
}
